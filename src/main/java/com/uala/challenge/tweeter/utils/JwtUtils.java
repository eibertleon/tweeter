package com.uala.challenge.tweeter.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.uala.challenge.tweeter.model.dto.JwtTokenDto;
import com.uala.challenge.tweeter.model.entity.UserEntity;
import com.uala.challenge.tweeter.utils.exception.InvalidCredentialsException;
import com.uala.challenge.tweeter.utils.exception.TweeterExceptionCode;

@Component
public class JwtUtils {

   @Value("${jwt.secret}")
   private String jwtSecret;

   @Value("${jwt.expiration}")
   private int jwtExpirationMs;

   public JwtTokenDto generateJwtToken(UserEntity userEntity) {

      Map<String, Object> claims = new HashMap<>();
      claims.put("subject", userEntity.getName());
      claims.put("password", userEntity.getPassword());
      claims.put("email", userEntity.getEmail());

      Claims jwtClaims = Jwts.claims(claims);
      jwtClaims.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs));
      jwtClaims.setIssuedAt(new Date());

      return new JwtTokenDto(Jwts
            .builder()
            .setClaims(claims)
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact());
   }

   public boolean validateJwtToken(String authToken) {
      try {
         Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
         return true;
      } catch (Exception e) {
         e.printStackTrace();
      }
      throw new InvalidCredentialsException(TweeterExceptionCode.UNAUTHORIZED);
   }


}
