package com.uala.challenge.tweeter.utils;

public enum StatusUserEnum {

   CREATED,
   BLOCKED,
   ERASED,
   ACTIVE

}
