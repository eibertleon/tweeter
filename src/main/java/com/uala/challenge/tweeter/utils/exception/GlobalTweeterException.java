package com.uala.challenge.tweeter.utils.exception;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class GlobalTweeterException extends RuntimeException {

   private TweeterExceptionCode tweeterExceptionCode;

   public GlobalTweeterException(TweeterExceptionCode tweeterExceptionCode) {
      this.tweeterExceptionCode = tweeterExceptionCode;
   }
   public GlobalTweeterException(String message, TweeterExceptionCode tweeterExceptionCode) {
      super(message);
      this.tweeterExceptionCode = tweeterExceptionCode;
   }

   @Override
   public String toString() {
      return tweeterExceptionCode + " :: " + super.toString();
   }

}
