package com.uala.challenge.tweeter.utils;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.uala.challenge.tweeter.model.entity.FollowEntity;
import com.uala.challenge.tweeter.model.entity.UserEntity;
import com.uala.challenge.tweeter.repository.FollowRepository;
import com.uala.challenge.tweeter.repository.UserRepository;

@Component
public class DataInitializer implements CommandLineRunner {

   private final UserRepository userRepository;

   private final FollowRepository followRepository;

   @Autowired
   public DataInitializer(UserRepository userRepository, FollowRepository followRepository) {
      this.userRepository = userRepository;
      this.followRepository = followRepository;
   }

   @Override
   public void run(String... args) throws Exception {
      for (int i = 1; i <= 5; i++) {
         UserEntity user = new UserEntity();
         user.setUserName("tweetero " + i);
         user.setEmail("tweetero" + i + "@email.com");
         user.setPassword(StringUtils.encodeHash("pass" + i));
         user.setName("Name" + i);
         user.setLastName("LastName" + i);
         user.setBirthDate(DateUtils.agregarAgnos(LocalDateTime.now(), -18+(-i)));
         userRepository.save(user);
      }

      for(int i = 1; i <= 5; i++) {
         Optional<UserEntity> userI = userRepository.findById(Long.valueOf(i));
         for(int j = 1; j <= 5; j++) {
            if(j != i) {
               Optional<UserEntity> userJ = userRepository.findById(Long.valueOf(j));
               FollowEntity followEntity = new FollowEntity();
               followEntity.setFollowed(userI.get());
               followEntity.setFollower(userJ.get());
               followRepository.save(followEntity);
            }
         }
      }

   }
}
