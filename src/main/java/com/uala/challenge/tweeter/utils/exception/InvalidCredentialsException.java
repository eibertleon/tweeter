package com.uala.challenge.tweeter.utils.exception;

public class InvalidCredentialsException extends GlobalTweeterException {

   public InvalidCredentialsException(TweeterExceptionCode tweeterExceptionCode) {
      super(tweeterExceptionCode);
   }

   public InvalidCredentialsException(String message, TweeterExceptionCode tweeterExceptionCode) {
      super(message, tweeterExceptionCode);
   }

}