package com.uala.challenge.tweeter.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;

public class DateUtils {

   public static LocalDateTime primerDiaDelMes(LocalDateTime fecha) {
      return fecha.withDayOfMonth(1);
   }

   public static LocalDateTime conDiaDelMes(LocalDateTime fecha, Integer dia) {
      return fecha.withDayOfMonth(dia);
   }

   public static LocalDateTime ultimoDiaDelMes(LocalDateTime fecha) {
      return fecha.withDayOfMonth(fecha.getMonth().length(Boolean.TRUE));
   }

   public static LocalDateTime agregarAgnos(LocalDateTime fecha, Integer numero) {
      return fecha.plusYears(Long.valueOf(numero));
   }
   public static LocalDateTime agregarMeses(LocalDateTime fecha, Integer numero) {
      return fecha.plusMonths(Long.valueOf(numero));
   }

   public static LocalDateTime agregarDias(LocalDateTime fecha, Integer numero) {
      return fecha.plusDays(Long.valueOf(numero));
   }

   public static Integer diasEntreFechas(LocalDateTime fecha1, LocalDateTime fecha2) {
      return Math.toIntExact(ChronoUnit.DAYS.between(fecha1, fecha2));
   }

   public static Integer annosEntreFechas(LocalDateTime fecha1, LocalDateTime fecha2) {
      return Math.toIntExact(ChronoUnit.YEARS.between(fecha1, fecha2));
   }

   public static LocalDateTime obtenerFechaSegundaCuota(){
      LocalDateTime hoy = LocalDateTime.now();
      LocalDateTime act25 = LocalDateTime.now().withDayOfMonth(25);
      LocalDateTime act10 = LocalDateTime.now().withDayOfMonth(10);
      if(hoy.isAfter(act25)){
         return agregarMeses(act10, 2);
      } else {
         return agregarMeses(act10, 1);
      }
   }

   public static String sdf(LocalDateTime fecha, String formato){
      SimpleDateFormat sdf = new SimpleDateFormat(formato);
      return fecha.format(DateTimeFormatter.ofPattern(formato));
   }

   public static LocalDateTime ldt(String fecha, String formato){
      LocalDateTime dateTime;
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formato);
      TemporalAccessor temporalAccessor = formatter.parseBest(fecha, LocalDateTime::from, LocalDate::from);
      if (temporalAccessor instanceof LocalDateTime) {
         return (LocalDateTime)temporalAccessor;
      } else {
         return ((LocalDate)temporalAccessor).atStartOfDay();
      }
   }

}
