package com.uala.challenge.tweeter.utils.handler;

import java.util.Objects;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.uala.challenge.tweeter.model.dto.ErrorResponseDto;
import com.uala.challenge.tweeter.utils.exception.TweeterExceptionCode;
import com.uala.challenge.tweeter.utils.exception.GlobalTweeterException;
import com.uala.challenge.tweeter.utils.exception.InvalidCredentialsException;

@RestControllerAdvice
public class RestControllerHandler {
   @ExceptionHandler(GlobalTweeterException.class)
   public ResponseEntity<ErrorResponseDto> handleChallengeException(GlobalTweeterException ex) {
      //TODO Aca se podria implementar un servicio de mensajeria.
      return createResponse(ex.getTweeterExceptionCode(), ex.getMessage());
   }

   @ExceptionHandler(InvalidCredentialsException.class)
   public ResponseEntity<ErrorResponseDto> handleChallengeException(InvalidCredentialsException ex) {
      //TODO Aca se podria implementar un servicio de mensajeria.
      return createResponse(ex.getTweeterExceptionCode(), ex.getMessage());
   }

   private ResponseEntity<ErrorResponseDto> createResponse(TweeterExceptionCode processorGatewayExceptionCode, String message) {
      if (Objects.nonNull(message) && !message.isEmpty()) {
         return new ResponseEntity<>(toError(processorGatewayExceptionCode, message), processorGatewayExceptionCode.getHttpStatus());
      }
      return new ResponseEntity<>(toError(processorGatewayExceptionCode), processorGatewayExceptionCode.getHttpStatus());
   }

   private ErrorResponseDto toError(TweeterExceptionCode tweeterExceptionCode, String message) {
      return new ErrorResponseDto(tweeterExceptionCode.getError(), message);
   }

   private ErrorResponseDto toError(TweeterExceptionCode tweeterExceptionCode) {
      return new ErrorResponseDto(tweeterExceptionCode.getError(), tweeterExceptionCode.getDescripcion());
   }


}
