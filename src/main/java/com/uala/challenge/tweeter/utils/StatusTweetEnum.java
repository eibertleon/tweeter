package com.uala.challenge.tweeter.utils;

public enum StatusTweetEnum {

   PUBLISHED,
   DELETED,
   HIDDEN

}
