package com.uala.challenge.tweeter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uala.challenge.tweeter.model.entity.TweetEntity;
import com.uala.challenge.tweeter.utils.StatusTweetEnum;

public interface TweetRepository extends JpaRepository<TweetEntity, Long> {

   @Query("SELECT t FROM TweetEntity t WHERE t.author.id = :userId")
   List<TweetEntity> findAllTweetsByUser(@Param("userId") Long userId);

   @Modifying
   @Query("UPDATE TweetEntity t SET t.status = :status WHERE t.id = :tweetId")
   void updateTweetStatus(@Param("tweetId") Long tweetId, @Param("status") StatusTweetEnum status);

}
