package com.uala.challenge.tweeter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uala.challenge.tweeter.model.entity.FollowEntity;

public interface FollowRepository extends JpaRepository<FollowEntity, Long> {

   @Query("SELECT t FROM FollowEntity t WHERE t.followed.id = :userId")
   List<FollowEntity> findAllFollowsByUserId(@Param("userId") Long userId);

   @Query("SELECT t FROM FollowEntity t WHERE t.follower.id = :userId")
   List<FollowEntity> findAllFollowingByUserId(@Param("userId") Long userId);

}
