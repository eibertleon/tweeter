package com.uala.challenge.tweeter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uala.challenge.tweeter.model.entity.RetweetEntity;

public interface RetweetRepository extends JpaRepository<RetweetEntity, Long> {

   @Query("SELECT t FROM RetweetEntity t WHERE t.retweeter.id = :userId")
   List<RetweetEntity> findAllRetweetsByUser(@Param("userId") Long userId);


}
