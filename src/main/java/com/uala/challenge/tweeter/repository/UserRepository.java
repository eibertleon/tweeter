package com.uala.challenge.tweeter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.uala.challenge.tweeter.model.entity.UserEntity;
import com.uala.challenge.tweeter.utils.StatusUserEnum;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

   Optional<UserEntity> findByEmailAndPassword(String email, String password);

   @Modifying
   @Query("UPDATE UserEntity t SET t.status = :status WHERE t.id = :userId")
   void updateUserStatus(@Param("userId") Long userId, @Param("status") StatusUserEnum status);


}
