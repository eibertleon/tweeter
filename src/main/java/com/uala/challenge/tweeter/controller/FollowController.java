package com.uala.challenge.tweeter.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uala.challenge.tweeter.model.dto.ErrorResponseDto;
import com.uala.challenge.tweeter.model.dto.FollowCreateDto;
import com.uala.challenge.tweeter.model.dto.FollowDto;
import com.uala.challenge.tweeter.service.FollowService;

@RestController
@RequestMapping("/api/private")
public class FollowController {

   @Autowired
   private FollowService followService;

   @PostMapping("/follow")
   @ResponseBody
   @Operation(summary = "Crear Follow", description = "Crear un nuevo Follow", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "201", description = "Json Follow Dto", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<FollowDto> createFollow(@RequestBody FollowCreateDto followCreateDto) {
      return new ResponseEntity<>(followService.createFollow(followCreateDto), HttpStatus.CREATED);
   }

   @DeleteMapping("/follow/{id}")
   @ResponseBody
   @Operation(summary = "Borrar Follow", description = "Eliminar el follow", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get Data View", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<Void> deleteFollow(@PathVariable Long followId) {
      return ResponseEntity.ok(followService.deleteFollow(followId));
   }

}
