package com.uala.challenge.tweeter.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.uala.challenge.tweeter.model.dto.ErrorResponseDto;
import com.uala.challenge.tweeter.model.dto.JwtTokenDto;
import com.uala.challenge.tweeter.model.dto.UserLoginDto;
import com.uala.challenge.tweeter.model.entity.UserEntity;
import com.uala.challenge.tweeter.service.UserService;
import com.uala.challenge.tweeter.utils.JwtUtils;

@RestController
@RequestMapping("/api/public/auth")
public class AuthController {

   @Autowired
   private UserService userService;

   @Autowired
   private JwtUtils jwtUtils;

   @PostMapping("/login")
   @ResponseBody
   @Operation(summary = "Ontener token - Login", description = "Obtener un token de acceso")
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get a token access", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = JwtTokenDto.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<JwtTokenDto> generateToken(@RequestBody UserLoginDto userLoginDto) {

      UserEntity userEntity = userService.findByEmailAndPassword(userLoginDto.getEmail(), userLoginDto.getPassword());

      return ResponseEntity.ok(jwtUtils.generateJwtToken(userEntity));
   }


}