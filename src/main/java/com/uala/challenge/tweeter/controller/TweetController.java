package com.uala.challenge.tweeter.controller;

import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uala.challenge.tweeter.model.dto.ErrorResponseDto;
import com.uala.challenge.tweeter.model.dto.TweetCreateDto;
import com.uala.challenge.tweeter.model.dto.TweetDto;
import com.uala.challenge.tweeter.service.TweetService;

@RestController
@RequestMapping("/api/private")
public class TweetController {

   @Autowired
   private TweetService tweetService;

   @PostMapping("/tweet")
   @ResponseBody
   @Operation(summary = "Crear Tweet", description = "Crear un nuevo tweet", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "201", description = "Json Tweet Dto", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<TweetDto> createTweet(@RequestBody TweetCreateDto tweetCreateDto) {
      return new ResponseEntity<>(tweetService.createTweet(tweetCreateDto), HttpStatus.CREATED);
   }

   @GetMapping("/tweet/{id}")
   @ResponseBody
   @Operation(summary = "Obtener tweet", description = "Obtener un tweet por id", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get Data View", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<TweetDto> getTweet(@PathVariable Long id) {
      return ResponseEntity.ok(tweetService.getTweetById(id));
   }

   @GetMapping("/tweet")
   @ResponseBody
   @Operation(summary = "Todas los tweets", description = "Obtener todas los tweets", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get Data View", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<List<TweetDto>> getAllTweets() {
      return ResponseEntity.ok(tweetService.getAllTweets());
   }

   @GetMapping("/tweet/user/{userId}")
   @ResponseBody
   @Operation(summary = "Todas los tweets", description = "Obtener todas los tweets de un usuario", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get Data View", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<List<TweetDto>> getAllTweetsByUser(@PathVariable Long userId) {
      return ResponseEntity.ok(tweetService.getAllTweetsByUser(userId));
   }

   @PutMapping("/tweet/{tweetId}/toHid")
   @ResponseBody
   @Operation(summary = "Ocultar Tweet", description = "Ocualtar el tweet", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get Data View", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<TweetDto> toHidTweet(@PathVariable Long tweetId) {
      return ResponseEntity.ok(tweetService.toHidTweet(tweetId));
   }

   @DeleteMapping("/tweet/{id}")
   @ResponseBody
   @Operation(summary = "Borrar Tweet", description = "Eliminar el tweet", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get Data View", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<TweetDto> deleteTweet(@PathVariable Long tweetId) {
      return ResponseEntity.ok(tweetService.deleteTweetLogically(tweetId));
   }

}
