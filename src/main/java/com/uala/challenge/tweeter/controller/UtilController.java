package com.uala.challenge.tweeter.controller;

import java.util.EnumSet;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uala.challenge.tweeter.utils.StatusTweetEnum;
import com.uala.challenge.tweeter.utils.StatusUserEnum;

@RequestMapping("/api/public")
public class UtilController {

   @GetMapping("/tweet/status-enum")
   public ResponseEntity<EnumSet> getStatusTweetEnums() {
      return ResponseEntity.ok(EnumSet.allOf(StatusTweetEnum.class));
   }

   @GetMapping("/user/status-enum")
   public ResponseEntity<EnumSet> getStatusUserEnums() {
      return ResponseEntity.ok(EnumSet.allOf(StatusUserEnum.class));
   }

}
