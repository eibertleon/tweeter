package com.uala.challenge.tweeter.controller;

import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uala.challenge.tweeter.model.dto.ErrorResponseDto;
import com.uala.challenge.tweeter.model.dto.RetweetCreateDto;
import com.uala.challenge.tweeter.model.dto.RetweetDto;
import com.uala.challenge.tweeter.service.RetweetService;

@RestController
@RequestMapping("/api/private")
public class RetweetController {

   @Autowired
   private RetweetService retweetService;

   @PostMapping("/retweet")
   @ResponseBody
   @Operation(summary = "Crear Retweet", description = "Crear un nuevo Retweet", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "201", description = "Json Retweet Dto", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<RetweetDto> createTweet(@RequestBody RetweetCreateDto retweetCreateDto) {
      return new ResponseEntity<>(retweetService.createRetweet(retweetCreateDto), HttpStatus.CREATED);
   }

   @GetMapping("/retweet/user/{userId}")
   @ResponseBody
   @Operation(summary = "Todas los retweets", description = "Obtener todas los retweets de un usuario", security = @SecurityRequirement(name = "Token"))
   @ApiResponses(value = { //
         @ApiResponse(responseCode = "200", description = "Get Data View", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema
               = @Schema(implementation = String.class))),
         @ApiResponse(responseCode = "DEFAULT", description = "Default error.", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
               schema = @Schema(implementation = ErrorResponseDto.class))) })
   public ResponseEntity<List<RetweetDto>> getAllRetweetsByUser(@PathVariable Long userId) {
      return ResponseEntity.ok(retweetService.getAllRetweetsByUser(userId));
   }


}
