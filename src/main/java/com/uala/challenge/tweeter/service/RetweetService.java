package com.uala.challenge.tweeter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uala.challenge.tweeter.model.dto.RetweetCreateDto;
import com.uala.challenge.tweeter.model.dto.RetweetDto;
import com.uala.challenge.tweeter.model.entity.RetweetEntity;
import com.uala.challenge.tweeter.model.mapper.RetweetMapper;
import com.uala.challenge.tweeter.repository.RetweetRepository;
import com.uala.challenge.tweeter.utils.exception.GlobalTweeterException;
import com.uala.challenge.tweeter.utils.exception.TweeterExceptionCode;

@Service
public class RetweetService {

   @Autowired
   private RetweetRepository retweetRepository;

   public RetweetDto createRetweet(RetweetCreateDto dto) {
      RetweetEntity entity = RetweetMapper.INSTANCE.toRetweetEntity(dto);
      return RetweetMapper.INSTANCE.toRetweeterDto(retweetRepository.save(entity));
   }

   public Void deleteRetweet(Long id) {
      if (!retweetRepository.existsById(id)) {
         throw new GlobalTweeterException("retweet entity", TweeterExceptionCode.NOT_FOUND);
      }
      retweetRepository.deleteById(id);
      return null;
   }

   public List<RetweetDto> getAllRetweetsByUser(Long userId) {
      List<RetweetEntity> retweets = retweetRepository.findAllRetweetsByUser(userId);
      if(!retweets.isEmpty()) {
         return RetweetMapper.INSTANCE.toRetweeterDto(retweets);
      }
      throw new GlobalTweeterException("Retweets", TweeterExceptionCode.NOT_FOUND);
   }

}
