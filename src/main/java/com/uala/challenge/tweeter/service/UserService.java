package com.uala.challenge.tweeter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uala.challenge.tweeter.model.dto.UserCreateDto;
import com.uala.challenge.tweeter.model.dto.UserDto;
import com.uala.challenge.tweeter.model.entity.UserEntity;
import com.uala.challenge.tweeter.model.mapper.UserMapper;
import com.uala.challenge.tweeter.repository.UserRepository;
import com.uala.challenge.tweeter.utils.StatusUserEnum;
import com.uala.challenge.tweeter.utils.StringUtils;
import com.uala.challenge.tweeter.utils.exception.GlobalTweeterException;
import com.uala.challenge.tweeter.utils.exception.TweeterExceptionCode;

@Service
public class UserService {

   @Autowired
   private UserRepository userRepository;

   @Autowired
   private FollowService followService;


   public UserEntity findByEmailAndPassword(String email, String clave) {
      return userRepository.findByEmailAndPassword(email, StringUtils.encodeHash(clave))
                              .orElseThrow(() -> new GlobalTweeterException(TweeterExceptionCode.NOT_FOUND));
   }

   public UserDto createUser(UserCreateDto dto) {
      UserEntity entity = UserMapper.INSTANCE.toUserEntity(dto);
      entity.setStatus(StatusUserEnum.CREATED);
      return UserMapper.INSTANCE.toUserDto(userRepository.save(entity));
   }

   public Void deleteUserLogically(Long id) {
      if (!userRepository.existsById(id)) {
         throw new GlobalTweeterException("User entity", TweeterExceptionCode.NOT_FOUND);
      }
      userRepository.updateUserStatus(id, StatusUserEnum.ERASED);
      return null;
   }

   public Void toBlockUser(Long id) {
      if (!userRepository.existsById(id)) {
         throw new GlobalTweeterException("User entity", TweeterExceptionCode.NOT_FOUND);
      }
      userRepository.updateUserStatus(id, StatusUserEnum.BLOCKED);
      return null;
   }

   public UserDto getUserById(Long id) {
      UserDto userDto = UserMapper.INSTANCE.toUserDto(userRepository.findById(id)
                                  .orElseThrow(() -> new GlobalTweeterException("User entity", TweeterExceptionCode.NOT_FOUND)));
      userDto.setFollowers(followService.getFollowersByUserId(userDto.getId()));
      userDto.setFollowing(followService.getFollowingByUserId(userDto.getId()));
      return userDto;
   }

   public List<UserDto> getAllUsers() {
      List<UserEntity> users = userRepository.findAll();
      if(!users.isEmpty()) {
         return UserMapper.INSTANCE.toUserDto(users);
      }
      throw new GlobalTweeterException("Users", TweeterExceptionCode.NOT_FOUND);
   }

}
