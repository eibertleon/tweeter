package com.uala.challenge.tweeter.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uala.challenge.tweeter.model.dto.FollowCreateDto;
import com.uala.challenge.tweeter.model.dto.FollowDto;
import com.uala.challenge.tweeter.model.dto.UserDto;
import com.uala.challenge.tweeter.model.entity.FollowEntity;
import com.uala.challenge.tweeter.model.mapper.FollowMapper;
import com.uala.challenge.tweeter.model.mapper.UserMapper;
import com.uala.challenge.tweeter.repository.FollowRepository;
import com.uala.challenge.tweeter.utils.exception.GlobalTweeterException;
import com.uala.challenge.tweeter.utils.exception.TweeterExceptionCode;

@Service
public class FollowService {

   @Autowired
   private FollowRepository followRepository;

   public FollowDto createFollow(FollowCreateDto dto) {
      FollowEntity entity = FollowMapper.INSTANCE.toFollowEntity(dto);
      return FollowMapper.INSTANCE.toFollowDto(followRepository.save(entity));
   }

   public Void deleteFollow(Long id) {
      if (!followRepository.existsById(id)) {
         throw new GlobalTweeterException("Follow entity", TweeterExceptionCode.NOT_FOUND);
      }
      followRepository.deleteById(id);
      return null;
   }

   public List<FollowDto> getFollowsByUserId(Long userId) {
      return FollowMapper.INSTANCE.toFollowDto(followRepository.findAllFollowsByUserId(userId));
   }

   public List<UserDto> getFollowersByUserId(Long userId) {
      List<FollowDto> follows = FollowMapper.INSTANCE.toFollowDto(followRepository.findAllFollowsByUserId(userId));
      return follows.stream().map(FollowDto::getFollower).collect(Collectors.toList());
   }

   public List<UserDto> getFollowingByUserId(Long userId) {
      List<FollowDto> follows = FollowMapper.INSTANCE.toFollowDto(followRepository.findAllFollowingByUserId(userId));
      return follows.stream().map(FollowDto::getFollowed).collect(Collectors.toList());
   }
}
