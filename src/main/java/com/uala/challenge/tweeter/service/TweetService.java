package com.uala.challenge.tweeter.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uala.challenge.tweeter.model.dto.TweetCreateDto;
import com.uala.challenge.tweeter.model.dto.TweetDto;
import com.uala.challenge.tweeter.model.entity.TweetEntity;
import com.uala.challenge.tweeter.model.mapper.TweetMapper;
import com.uala.challenge.tweeter.repository.TweetRepository;
import com.uala.challenge.tweeter.utils.StatusTweetEnum;
import com.uala.challenge.tweeter.utils.exception.TweeterExceptionCode;
import com.uala.challenge.tweeter.utils.exception.GlobalTweeterException;

@Service
public class TweetService {

   @Autowired
   private TweetRepository tweetRepository;

   public TweetDto createTweet(TweetCreateDto dto) {
      TweetEntity entity = TweetMapper.INSTANCE.toTweetEntity(dto);
      entity.setStatus(StatusTweetEnum.PUBLISHED);
      return TweetMapper.INSTANCE.toTweetDto(tweetRepository.save(entity));
   }

   public TweetDto deleteTweetLogically(Long id) {
      if (!tweetRepository.existsById(id)) {
         throw new GlobalTweeterException("Tweet entity", TweeterExceptionCode.NOT_FOUND);
      }
      tweetRepository.updateTweetStatus(id, StatusTweetEnum.DELETED);
      return this.getTweetById(id);
   }

   public TweetDto toHidTweet(Long id) {
      if (!tweetRepository.existsById(id)) {
         throw new GlobalTweeterException("Tweet entity", TweeterExceptionCode.NOT_FOUND);
      }
      tweetRepository.updateTweetStatus(id, StatusTweetEnum.HIDDEN);
      return this.getTweetById(id);
   }

   public TweetDto getTweetById(Long id) {
      return TweetMapper.INSTANCE.toTweetDto(tweetRepository.findById(id)
                        .orElseThrow(() -> new GlobalTweeterException("Tweet entity", TweeterExceptionCode.NOT_FOUND)));
   }

   public List<TweetDto> getAllTweets() {
      List<TweetEntity> users = tweetRepository.findAll();
      if(!users.isEmpty()) {
         return TweetMapper.INSTANCE.toTweetDto(users);
      }
      throw new GlobalTweeterException("Tweets", TweeterExceptionCode.NOT_FOUND);
   }

   public List<TweetDto> getAllTweetsByUser(Long userId) {
      List<TweetEntity> tweets = tweetRepository.findAllTweetsByUser(userId);
      if(!tweets.isEmpty()) {
         return TweetMapper.INSTANCE.toTweetDto(tweets);
      }
      throw new GlobalTweeterException("Tweets", TweeterExceptionCode.NOT_FOUND);
   }

}
