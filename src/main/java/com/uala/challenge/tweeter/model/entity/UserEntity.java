package com.uala.challenge.tweeter.model.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.uala.challenge.tweeter.utils.StatusUserEnum;

import lombok.Data;

@Data
@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class UserEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   private String userName;
   private String email;
   private String password;
   @CreatedDate
   private LocalDateTime registrationDate;
   private String name;
   private String lastName;
   private LocalDateTime birthDate;
   private StatusUserEnum status;
   @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
   private List<TweetEntity> tweets;
   @OneToMany(mappedBy = "retweeter", cascade = CascadeType.ALL)
   private List<RetweetEntity> retweets;

}
