package com.uala.challenge.tweeter.model.mapper;

import java.util.List;

import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import com.uala.challenge.tweeter.model.dto.FollowCreateDto;
import com.uala.challenge.tweeter.model.dto.FollowDto;
import com.uala.challenge.tweeter.model.entity.FollowEntity;

@Mapper(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED, uses = UserMapper.class)
@Named("followMapper")
public interface FollowMapper {

   FollowMapper INSTANCE = Mappers.getMapper(FollowMapper.class);

   @Named("toFollowDto")
   @Mapping(target = "follower", qualifiedByName = { "userMapper", "toUserDto" })
   @Mapping(target = "followed", qualifiedByName = { "userMapper", "toUserDto" })
   FollowDto toFollowDto(FollowEntity entity);

   @Named("toFollowDtoList")
   @IterableMapping(qualifiedByName="toFollowDto")
   List<FollowDto> toFollowDto(List<FollowEntity> entity);

   @Mapping(target = "follower.id", source = "follower")
   @Mapping(target = "followed.id", source = "followed")
   FollowEntity toFollowEntity(FollowCreateDto dto);

}
