package com.uala.challenge.tweeter.model.mapper;

import java.util.List;

import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import com.uala.challenge.tweeter.model.dto.RetweetCreateDto;
import com.uala.challenge.tweeter.model.dto.RetweetDto;
import com.uala.challenge.tweeter.model.entity.RetweetEntity;

@Mapper(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED, uses = {TweetMapper.class, UserMapper.class})
@Named("retweeterMapper")
public interface RetweetMapper {

   RetweetMapper INSTANCE = Mappers.getMapper(RetweetMapper.class);

   @Named("toRetweeterDto")
   @Mapping( target = "retweeter", qualifiedByName = { "userMapper", "toUserDto" } )
   @Mapping( target = "tweet", qualifiedByName = { "tweetMapper", "toTweetDto" } )
   RetweetDto toRetweeterDto(RetweetEntity entity);

   @Named("toRetweeterDtoList")
   @IterableMapping(qualifiedByName="toRetweeterDto")
   List<RetweetDto> toRetweeterDto(List<RetweetEntity> entity);

   @Mapping(target = "retweeter.id", source = "retweeter")
   @Mapping(target = "tweet.id", source = "tweet")
   RetweetEntity toRetweetEntity(RetweetCreateDto dto);

}
