package com.uala.challenge.tweeter.model.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingInheritanceStrategy;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import com.uala.challenge.tweeter.model.dto.TweetCreateDto;
import com.uala.challenge.tweeter.model.dto.TweetDto;
import com.uala.challenge.tweeter.model.entity.TweetEntity;

@Mapper(mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_ALL_FROM_CONFIG)
@Named("tweetMapper")
public interface TweetMapper {

   TweetMapper INSTANCE = Mappers.getMapper(TweetMapper.class);

   @Named("toTweetDto")
   @Mapping(target = "author", source = "author.id")
   TweetDto toTweetDto(TweetEntity entity);

   @Named("toTweetDtoList")
   @IterableMapping(qualifiedByName="toTweetDto")
   List<TweetDto> toTweetDto(List<TweetEntity> entity);

   @Mapping(target = "author.id", source = "author")
   TweetEntity toTweetEntity(TweetCreateDto dto);

}
