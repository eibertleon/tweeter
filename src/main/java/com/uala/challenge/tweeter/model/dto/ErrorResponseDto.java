package com.uala.challenge.tweeter.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponseDto {

   private String error;

   private String description;


}
