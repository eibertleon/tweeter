package com.uala.challenge.tweeter.model.mapper;

import java.util.List;

import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import com.uala.challenge.tweeter.model.dto.UserCreateDto;
import com.uala.challenge.tweeter.model.dto.UserDto;
import com.uala.challenge.tweeter.model.entity.UserEntity;

@Mapper(collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED, uses = TweetMapper.class)
@Named("userMapper")
public interface UserMapper {

   UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

   @Named("toUserDto")
   @Mapping( target = "tweets", qualifiedByName = { "tweetMapper", "toTweetDtoList" } )
   UserDto toUserDto(UserEntity entity);

   @Named("toUserDtoList")
   @IterableMapping(qualifiedByName="toUserDto")
   List<UserDto> toUserDto(List<UserEntity> entity);

   UserEntity toUserEntity(UserCreateDto dto);

}
