package com.uala.challenge.tweeter.model.entity;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@Entity
@Table(name = "retweets")
@EntityListeners(AuditingEntityListener.class)
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class RetweetEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @ManyToOne
   private UserEntity retweeter;

   @ManyToOne
   private TweetEntity tweet;

}
