package com.uala.challenge.tweeter.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.uala.challenge.tweeter.utils.StatusTweetEnum;

import lombok.Data;

@Data
@Entity
@Table(name = "tweets")
@EntityListeners(AuditingEntityListener.class)
@JsonNaming(PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class TweetEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   private String content;
   private StatusTweetEnum status;
   @ManyToOne
   private UserEntity author;
   @CreatedDate
   private LocalDateTime creationDate;
   @LastModifiedDate
   private LocalDateTime modifiedDate;


}
